// On load initialize Functions
(function() {
})();

document.onreadystatechange = function () {
  $('#keyboard').jkeyboard({
    layout: "english_capital",
    input: $('#search_field'),
    customLayouts: {
      selectable: ["english_capital"],
      english_capital: [
      [ '1', '2', '3', '4', '5', '6', '7', '8', '9', '0','backspace', 'clear'],
      ['Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '@', '&', '+' ],
      ['A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', '-', '/', '\''],
      ['Z', 'X', 'C', 'V', 'B', 'N', 'M', ',', '.', ':', 'space'],
      ],
    }
  });
}

function keyboardFocus( target ){
  document.getElementById("nav-bar").querySelectorAll("li")[2].click();
  document.getElementById("footer").classList.add('active');
}

function closeKeyboard(){
  document.getElementById("footer").classList.remove('active');
  document.getElementById("search_field").value = '';
}

function navSelection(target) {
  for (var item of document.getElementById("nav-bar").querySelectorAll("li")) {
    item.classList.remove("active");
  }
  target.classList.add("active");
  document.getElementById("footer").classList.remove('active');
}

function selectCategory(target){
  for (var item of document.getElementById("category").querySelectorAll("div")) {
    item.classList.remove("active");
  }
  target.classList.add("active");
}

function selectLevel(target){
  for (var item of document.getElementById("level").querySelectorAll("div")) {
    item.classList.remove("active");
  }
  target.classList.add("active");
}
// Home Page Function
function pageHome(target) {
  var mainWrapper = document.getElementById("mainWrapper");
  mainWrapper.className = "home mainbody w-100";

  var output_html = `
    <img src="images/home/banner.jpg" class="w-100 h-100">
    `;
  document.getElementById("mainWrapper").innerHTML = output_html;
  navSelection(target);
}

// Floor Page Function
function pageFloor(target) {
  var mainWrapper = document.getElementById("mainWrapper");
  mainWrapper.className = "floor mainbody w-100";
  var title = "L1";

  var output_html = `
    <h1 class="mb-5 level">${title}</h1>
    <div class="text-center">
      <img src="images/floor/floor.png" class="m-auto">
    </div>
    <div class="text-center floorList" id="level">
      <ul>
      <li> <p> B1 </p> </li>
      <li class="active"> <p> L1 </p> </li>
      <li> <p> L2 </p> </li>
      <li> <p> L3 </p></li>
      <li> <p> L4 </p> </li>
      </ul>
    </div>
    `;
  document.getElementById("mainWrapper").innerHTML = output_html;

  navSelection(target);
}

// Shop Page Function
function pageShop(target) {
  var mainWrapper = document.getElementById("mainWrapper");
  mainWrapper.className = "fnb mainbody w-100";
  var title = "Shop";

  var output_html = ` 
<div class="p-5" id="fnb">
  <h1 class="mb-5">${title}</h1>
  <div class="category text-center d-none">
    <img src="./images/fnb/FnB.jpg" />
  </div>
  <div class="tab-content" id="myTabContent">
  <div class="tab-pane fade active show" id="toristDelight" role="tabpanel" aria-labelledby="toristDelight-tab">

  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
    <div class="list-wrapper">
    <div class="carouselBlock">
      <div class="row">
        <div class="block col"><img src="./images/fnb/tenants/1.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/2.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/3.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/4.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/5.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
      </div>
      <div class="row">
        <div class="block col"><img src="./images/fnb/tenants/6.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/7.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/8.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/9.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/10.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
      </div>
    </div>
    </div>
    </div>
    <div class="carousel-item">
    <div class="list-wrapper">
    <div class="carouselBlock">
      <div class="row">
        <div class="block col"><img src="./images/fnb/tenants/1.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/2.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/3.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/4.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/5.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
      </div>
      <div class="row">
        <div class="block col"><img src="./images/fnb/tenants/6.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/7.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/8.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/9.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/10.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
      </div>
    </div>
    </div>
    </div>
    <div class="carousel-item">
    <div class="list-wrapper">
    <div class="carouselBlock">
      <div class="row">
        <div class="block col"><img src="./images/fnb/tenants/1.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/2.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/3.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/4.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/5.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
      </div>
      <div class="row">
        <div class="block col"><img src="./images/fnb/tenants/6.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/7.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/8.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/9.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/10.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
      </div>
    </div>
    </div>
    </div>
  </div>
</div>

  </div>
  <div class="tab-pane fade" id="shuttleBus" role="tabpanel" aria-labelledby="shuttleBus-tab">
  
  <div id="carouselExampleIndicator_2" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicator_2" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicator_2" data-slide-to="1"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
    <div class="list-wrapper">
    <div class="carouselBlock">
      <div class="row">
        <div class="block col"><img src="./images/fnb/tenants/1.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/2.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/3.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/4.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/5.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
      </div>
      <div class="row">
        <div class="block col"><img src="./images/fnb/tenants/6.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/7.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/8.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/9.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/10.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
      </div>
    </div>
    </div>
    </div>
    <div class="carousel-item">
    <div class="list-wrapper">
    <div class="carouselBlock">
      <div class="row">
        <div class="block col"><img src="./images/fnb/tenants/1.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/2.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/3.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/4.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/5.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
      </div>
      <div class="row">
        <div class="block col"><img src="./images/fnb/tenants/6.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/7.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/8.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/9.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/10.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
      </div>
    </div>
    </div>
    </div>
  </div>
</div>
  </div>
  <div class="tab-pane fade" id="nursingRoom" role="tabpanel" aria-labelledby="nursingRoom-tab">  
  
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
    <div class="list-wrapper">
    <div class="carouselBlock">
      <div class="row">
        <div class="block col"><img src="./images/fnb/tenants/1.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/2.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/3.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/4.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/5.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
      </div>
      <div class="row">
        <div class="block col"><img src="./images/fnb/tenants/6.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/7.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/8.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/9.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/10.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
      </div>
    </div>
    </div>
    </div>
    <div class="carousel-item">
    <div class="list-wrapper">
    <div class="carouselBlock">
      <div class="row">
        <div class="block col"><img src="./images/fnb/tenants/1.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/2.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/3.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/4.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/5.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
      </div>
      <div class="row">
        <div class="block col"><img src="./images/fnb/tenants/6.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/7.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/8.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/9.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/10.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
      </div>
    </div>
    </div>
    </div>
    <div class="carousel-item">
    <div class="list-wrapper">
    <div class="carouselBlock">
      <div class="row">
        <div class="block col"><img src="./images/fnb/tenants/1.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/2.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/3.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/4.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/5.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
      </div>
      <div class="row">
        <div class="block col"><img src="./images/fnb/tenants/6.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/7.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/8.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/9.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/10.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
      </div>
    </div>
    </div>
    </div>
  </div>
</div>
  </div>
</div>

<ul class="nav nav-tabs col-10" id="myTab" role="tablist">
<li class=" col nav-item" onclick="tabChange(this)">
  <a class="nav-link active" id="toristDelight-tab" data-targetCategory="all" data-toggle="tab" href="#toristDelight" role="tab" aria-controls="toristDelight" aria-selected="true">All</a>
</li>
<li class=" col nav-item" onclick="tabChange(this)">
  <a class="nav-link" id="shuttleBus-tab" data-targetCategory="category" data-toggle="tab" href="#shuttleBus" role="tab" aria-controls="shuttleBus" aria-selected="false">Category </a>
</li>
<li class=" col nav-item" onclick="tabChange(this)">
  <a class="nav-link" id="nursingRoom-tab" data-targetCategory="level" data-toggle="tab" href="#nursingRoom" role="tab" aria-controls="nursingRoom" aria-selected="false">Level</a>
</li>
</ul>


<div id="category" class="hideMenu">
  <div class="active" onclick="selectCategory(this)"> <span> Beauty & Wellness </span> </div>
  <div onclick="selectCategory(this)"> <span> Supermarket & Convenience Store </span> </div>
  <div onclick="selectCategory(this)"> <span> Fashion & Accessories </span> </div>
  <div onclick="selectCategory(this)"> <span> Education & Enrichment </span> </div>
  <div onclick="selectCategory(this)"> <span> Sports & Lifestyle </span> </div>
  <div onclick="selectCategory(this)"> <span> Services & Others </span> </div>
</div>

<div id="level" class="hideMenu">
  <div onclick="selectLevel(this)"> <span> B1 </span> </div>
  <div onclick="selectLevel(this)" class="active"> <span> L1 </span> </div>
  <div onclick="selectLevel(this)"> <span> L2 </span> </div>
  <div onclick="selectLevel(this)"> <span> L3 </span> </div>
  <div onclick="selectLevel(this)"> <span> L4 </span> </div>
</div>


</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
       <img src="./images/fnb/tenants/popup.png"/>
       <div onclick="wayfinding()" id="wayfindingbtn"> <img src="./images/wayfinding_btn.png"/> </div>
      </div>
      <div class="modal-footer">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      </div>
    </div>
  </div>
</div>


<!-- Wayfinding Modal -->
<div class="modal fade" id="wayfinding" tabindex="-1" role="dialog" aria-labelledby="wayfindingLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body text-center">
       <img src="./images/wayfinding_popup.jpg" onclick="wayfindingFloor()"/>
      </div>
      <div class="modal-footer">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      </div>
    </div>
  </div>
</div>
    `;
  document.getElementById("mainWrapper").innerHTML = output_html;
  $("#myTabContent").tab("dispose");
  $("#myTabContent").tab("show");

  navSelection(target);
}

// F&B Page Function
function pageFnB(target) {
  var mainWrapper = document.getElementById("mainWrapper");
  mainWrapper.className = "fnb mainbody w-100";
  var title = "F&B";

  var output_html = ` 
<div class="p-5" id="fnb">
  <h1 class="mb-5">${title}</h1>
  <div class="category text-center" id="dineLanding">
    <img src="./images/fnb/FnB.jpg" onclick="showDine()" />
  </div>
  <div class="tab-content d-none" id="myTabContent">
  <div class="tab-pane fade active show" id="toristDelight" role="tabpanel" aria-labelledby="toristDelight-tab">

  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
    <div class="list-wrapper">
    <div class="carouselBlock">
      <div class="row">
        <div class="block col"><img src="./images/fnb/tenants/1.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/2.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/3.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/4.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/5.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
      </div>
      <div class="row">
        <div class="block col"><img src="./images/fnb/tenants/6.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/7.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/8.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/9.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/10.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
      </div>
    </div>
    </div>
    </div>
    <div class="carousel-item">
    <div class="list-wrapper">
    <div class="carouselBlock">
      <div class="row">
        <div class="block col"><img src="./images/fnb/tenants/1.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/2.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/3.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/4.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/5.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
      </div>
      <div class="row">
        <div class="block col"><img src="./images/fnb/tenants/6.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/7.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/8.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/9.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/10.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
      </div>
    </div>
    </div>
    </div>
    <div class="carousel-item">
    <div class="list-wrapper">
    <div class="carouselBlock">
      <div class="row">
        <div class="block col"><img src="./images/fnb/tenants/1.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/2.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/3.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/4.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/5.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
      </div>
      <div class="row">
        <div class="block col"><img src="./images/fnb/tenants/6.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/7.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/8.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/9.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/10.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
      </div>
    </div>
    </div>
    </div>
  </div>
</div>

  </div>
  <div class="tab-pane fade" id="shuttleBus" role="tabpanel" aria-labelledby="shuttleBus-tab">
  
  <div id="carouselExampleIndicators_2" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators_2" data-slide-to="0" class="active"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
    <div class="list-wrapper">
    <div class="carouselBlock">
      <div class="row">
        <div class="block col"><img src="./images/fnb/tenants/1.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/2.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/3.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/4.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/5.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
      </div>
      <div class="row">
        <div class="block col"><img src="./images/fnb/tenants/6.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/7.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/8.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/9.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/10.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
      </div>
    </div>
    </div>
    </div>
  </div>
  </div>

  </div>
  <div class="tab-pane fade" id="nursingRoom" role="tabpanel" aria-labelledby="nursingRoom-tab">  
  <div id="carouselExampleIndicators_3" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators_3" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators_3" data-slide-to="1"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
    <div class="list-wrapper">
    <div class="carouselBlock">
      <div class="row">
        <div class="block col"><img src="./images/fnb/tenants/1.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/2.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/3.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/4.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/5.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
      </div>
      <div class="row">
        <div class="block col"><img src="./images/fnb/tenants/6.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/7.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/8.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/9.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/10.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
      </div>
    </div>
    </div>
    </div>
    <div class="carousel-item">
    <div class="list-wrapper">
    <div class="carouselBlock">
      <div class="row">
        <div class="block col"><img src="./images/fnb/tenants/1.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/2.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/3.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/4.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/5.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
      </div>
      <div class="row">
        <div class="block col"><img src="./images/fnb/tenants/6.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/7.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/8.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/9.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
        <div class="block col"><img src="./images/fnb/tenants/10.jpg" data-toggle="modal" data-target="#exampleModal" /> </div>
      </div>
    </div>
    </div>
    </div>
  </div>
</div>
  </div>
</div>

<ul class="nav nav-tabs col-10 d-none" id="myTab" role="tablist">
  <li class="col nav-item" onclick="tabChange(this)">
    <a class="nav-link active" id="toristDelight-tab" data-targetCategory="all" data-toggle="tab" href="#toristDelight" role="tab" aria-controls="toristDelight" aria-selected="true">All</a>
  </li>
  <li class="col nav-item" onclick="tabChange(this)">
    <a class="nav-link" id="shuttleBus-tab" data-toggle="tab" data-targetCategory="category" href="#shuttleBus" role="tab" aria-controls="shuttleBus" aria-selected="false">Category </a>
  </li>
  <li class="col nav-item" onclick="tabChange(this)">
    <a class="nav-link" id="nursingRoom-tab" data-toggle="tab" data-targetCategory="level" href="#nursingRoom" role="tab" aria-controls="nursingRoom" aria-selected="false">Level</a>
  </li>
</ul>

<div id="category" class="hideMenu">
  <div class="active" onclick="selectCategory(this)"> <span> Restaurants </span> </div>
  <div onclick="selectCategory(this)"> <span> Casual Dining & Cafes </span> </div>
  <div onclick="selectCategory(this)"> <span> Food Kiosk & Takeaways </span> </div>
</div>

<div id="level" class="hideMenu">
  <div onclick="selectLevel(this)"> <span> B1 </span> </div>
  <div onclick="selectLevel(this)" class="active"> <span> L1 </span> </div>
  <div onclick="selectLevel(this)"> <span> L2 </span> </div>
  <div onclick="selectLevel(this)"> <span> L3 </span> </div>
  <div onclick="selectLevel(this)"> <span> L4 </span> </div>
</div>

</div>

<!-- popup Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
       <img src="./images/fnb/tenants/popup.png"/>
       <div onclick="wayfinding()" id="wayfindingbtn"> <img src="./images/wayfinding_btn.png"/> </div>
      </div>
      <div class="modal-footer">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      </div>
    </div>
  </div>
</div>

<!-- Wayfinding Modal -->
<div class="modal fade" id="wayfinding" tabindex="-1" role="dialog" aria-labelledby="wayfindingLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body text-center">
       <img src="./images/wayfinding_popup.jpg" onclick="wayfindingFloor()"/>
      </div>
      <div class="modal-footer">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      </div>
    </div>
  </div>
</div>

    `;
  document.getElementById("mainWrapper").innerHTML = output_html;
  $("#myTabContent").tab("dispose");
  $("#myTabContent").tab("show");

  $('#carouselExampleIndicators_2').carousel();

  navSelection(target);
}

function showDine(){
  document.getElementById("dineLanding").classList.add("d-none");
  document.getElementById("myTabContent").classList.remove("d-none");
  document.getElementById("myTab").classList.remove("d-none");
}

function wayfinding(){
  $("#exampleModal").modal('hide');
  $("#wayfinding").modal('show');
}

function wayfindingFloor(){
  
  $("#wayfinding").modal('hide');

  document.getElementById("nav-bar").querySelectorAll("li")[1].click();
  var mainWrapper = document.getElementById("mainWrapper");
  mainWrapper.className = "floor mainbody w-100";

  var output_html = `
    <div class="text-center">
      <img src="images/wayfinding_floor.jpg" class="m-auto">
    </div>
    <div class="text-center" id="floorList">
      <ul>
      <li> <p> B1 </p> </li>
      <li class="active"> <p> L1 </p> </li>
      <li> <p> L2 </p> </li>
      <li> <p> L3 </p></li>
      <li> <p> L4 </p> </li>
      </ul>
    </div>
    `;
  document.getElementById("mainWrapper").innerHTML = output_html;

}

// Service Page Function
function pageServices(target) {
  var mainWrapper = document.getElementById("mainWrapper");
  mainWrapper.className = "service mainbody w-100";
  var title = "SERVICES";

  var output_html = ` 
  <div class="p-5">
    <h1 class="mb-5">${title}</h1>
      <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade active show" id="toristDelight" role="tabpanel" aria-labelledby="toristDelight-tab">
        <img src="./images/services/whats_on.jpg" />
        </div>
        <div class="tab-pane fade" id="shuttleBus" role="tabpanel" aria-labelledby="shuttleBus-tab">
        <img src="./images/services/whats_on.jpg" />
        </div>
        <div class="tab-pane fade" id="nursingRoom" role="tabpanel" aria-labelledby="nursingRoom-tab">  
        <img src="./images/services/whats_on.jpg" />
        </div>
        <div class="tab-pane fade" id="fareastMallVoucher" role="tabpanel" aria-labelledby="fareastMallVoucher-tab">  
        <img src="./images/services/whats_on.jpg" />
        </div>
        <div class="tab-pane fade" id="concierge" role="tabpanel" aria-labelledby="concierge-tab">  
        <img src="./images/services/whats_on.jpg" />
        </div>
        <div class="tab-pane fade" id="fareastMallVoucher" role="tabpanel" aria-labelledby="fareastMallVoucher-tab">  
        <img src="./images/services/whats_on.jpg" />
        </div>
        <div class="tab-pane fade" id="LOVAS" role="tabpanel" aria-labelledby="LOVAS-tab">  
        <img src="./images/services/whats_on.jpg" />
        </div>
      </div>

      <ul class="nav nav-tabs col-10" id="myTab" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" id="toristDelight-tab" data-toggle="tab" href="#toristDelight" role="tab" aria-controls="toristDelight" aria-selected="true">Torist Delight</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="shuttleBus-tab" data-toggle="tab" href="#shuttleBus" role="tab" aria-controls="shuttleBus" aria-selected="false">Shuttle </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="nursingRoom-tab" data-toggle="tab" href="#nursingRoom" role="tab" aria-controls="nursingRoom" aria-selected="false">Nursing Room</a>
      </li>
      <li class="nav-item">
      <a class="nav-link" id="fareastMallVoucher-tab" data-toggle="tab" href="#fareastMallVoucher" role="tab" aria-controls="fareastMallVoucher" aria-selected="false">Far East Mall Voucher</a>
      </li>
      <li class="nav-item">
      <a class="nav-link" id="concierge-tab" data-toggle="tab" href="#concierge" role="tab" aria-controls="concierge" aria-selected="false">Concierge</a>
      </li>
      <li class="nav-item">
      <a class="nav-link" id="LOVAS-tab" data-toggle="tab" href="#LOVAS" role="tab" aria-controls="LOVAS" aria-selected="false">Loan of Valued Add Services</a>
      </li>
      </ul>
  </div>
    `;
  document.getElementById("mainWrapper").innerHTML = output_html;

  $("#myTabContent").tab("dispose");
  $("#myTabContent").tab("show");

  navSelection(target);
}

// Transport Page Function
function pageTransport(target) {
  var mainWrapper = document.getElementById("mainWrapper");
  mainWrapper.className = "transport mainbody w-100";
  var title = "TRANSPORT";

  var output_html = ` 
<div class="p-5">
  <h1 class="mb-5">${title}</h1>
    <div class="tab-content" id="myTabContent">
      <div class="tab-pane fade active show" id="bus" role="tabpanel" aria-labelledby="bus-tab">
      <img src="./images/transport/transport.jpg" />
      </div>
      <div class="tab-pane fade" id="mrt" role="tabpanel" aria-labelledby="mrt-tab">
      <img src="./images/transport/transport.jpg" />
      </div>
      <div class="tab-pane fade" id="taxi" role="tabpanel" aria-labelledby="taxi-tab">  
      <img src="./images/transport/transport.jpg" />
      </div>
      <div class="tab-pane fade" id="parkingCharges" role="tabpanel" aria-labelledby="parkingCharges-tab">  
      <img src="./images/transport/transport.jpg" />
      </div>
    </div>

    <ul class="nav nav-tabs col-10" id="myTab" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" id="bus-tab" data-toggle="tab" href="#bus" role="tab" aria-controls="bus" aria-selected="true">Bus</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="mrt-tab" data-toggle="tab" href="#mrt" role="tab" aria-controls="mrt" aria-selected="false">MRT</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="taxi-tab" data-toggle="tab" href="#taxi" role="tab" aria-controls="taxi" aria-selected="false">Taxi</a>
    </li>
    <li class="nav-item">
    <a class="nav-link" id="parkingCharges-tab" data-toggle="tab" href="#parkingCharges" role="tab" aria-controls="parkingCharges" aria-selected="false">Parking Charges</a>
    </li>
    </ul>
</div>
    `;
  document.getElementById("mainWrapper").innerHTML = output_html;

  $("#myTabContent").tab("dispose");
  $("#myTabContent").tab("show");

  navSelection(target);
}

// What's on Page Function
function pageWhatson(target) {
  var mainWrapper = document.getElementById("mainWrapper");
  mainWrapper.className = "whatson mainbody w-100";
  var title = "What's On";

  var output_html = ` 
  <div class="p-5">
    <h1 class="mb-5">${title}</h1>
		<div class="slider">
			<div class="slick-slider-whatson">
				<div>
					<a data-toggle="modal" data-target="#whatson-main" data-src="./images/whatson/1.jpg" tabindex="0" onclick="popup(this)">
						<img src="./images/whatson/1.jpg">
					</a>
				</div>	
				<div>
					<a data-toggle="modal" data-target="#whatson-main" data-src="./images/whatson/2.jpg" tabindex="-1" onclick="popup(this)">
						<img src="./images/whatson/2.jpg">
					</a>
        </div>
        <div>
        <a data-toggle="modal" data-target="#whatson-main" data-src="./images/whatson/1.jpg" tabindex="-1" onclick="popup(this)">
          <img src="./images/whatson/1.jpg">
        </a>
        </div>
				<div>
					<a data-toggle="modal" data-target="#whatson-main" data-src="./images/whatson/3.jpg" tabindex="-1" onclick="popup(this)">
						<img src="./images/whatson/3.jpg">
					</a>
        </div>
			</div>
		</div>
  </div>


  <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body row p-0">
       <div class="col-6 p-0">
        <img src="./images/fnb/tenants/popup.png"/>
       </div>
       <div class="col-6 p-5">
        <h1> Stylish Gift </h1>
        <h5> Date: 10 December 2019 - 1 January 2020 </h5>
        <div> 
          Loren ipsum
        </div>
        <div onclick="wayfinding()" id="wayfindingbtn"> <img src="./images/wayfinding_btn.png"/> </div>
       </div>
      </div>
      <div class="modal-footer">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      </div>
    </div>
  </div>
</div>

<!-- Wayfinding Modal -->
<div class="modal fade" id="wayfinding" tabindex="-1" role="dialog" aria-labelledby="wayfindingLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body text-center">
       <img src="./images/wayfinding_popup.jpg" onclick="wayfindingFloor()"/>
      </div>
      <div class="modal-footer">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
      </div>
    </div>
  </div>
</div>

  `;

  document.getElementById("mainWrapper").innerHTML = output_html;

  $(".slick-slider-whatson").slick({
    centerMode: true,
    centerPadding: "500px",
    slidesToShow: 1,
    dots: true,
    arrows: false,
  });

  navSelection(target);
}

function popup(target) {
  document.getElementsByClassName("modal-body")[0].getElementsByTagName("img")[0].src = target.dataset.src;
  $("#exampleModal").modal("show");
}

function tabChange(target) {
  var categoryDivID = target.getElementsByTagName("a")[0].dataset.targetcategory;
  for (var item of document.querySelectorAll(".hideMenu")) {
    item.classList.remove("active");
  }
  document.getElementById(categoryDivID).classList.add("active");
}

